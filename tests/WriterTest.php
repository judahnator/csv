<?php declare(strict_types=1);

namespace Judahnator\CSV\Tests;

use Judahnator\CSV\Writer;
use PHPUnit\Framework\TestCase;
use RuntimeException;
use SplFileInfo;

/**
 * @covers \Judahnator\CSV\Writer
 */
final class WriterTest extends TestCase
{
    private const TEST_FILE_PREFIX = 'write_test_';

    public function testConstructionException(): void
    {
        $file = new SplFileInfo(tempnam(sys_get_temp_dir(), self::TEST_FILE_PREFIX));
        chmod($file->getRealPath(), 0400); // read only

        $this->expectExceptionObject(new RuntimeException('The file at ' . $file->getPath() . ' is not writable!'));

        new Writer($file);
    }

    public function testWrite(): void
    {
        $file = new SplFileInfo(tempnam(sys_get_temp_dir(), self::TEST_FILE_PREFIX));

        (new Writer($file))->write([
            ['foo', 'bar'],
            ['bing', 'baz'],
            ['one', 'two'],
        ]);

        $this->assertEquals(
            <<<CSV
            foo,bar
            bing,baz
            one,two
            
            CSV,
            file_get_contents($file->getRealPath()),
        );
    }
}
