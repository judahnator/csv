<?php declare(strict_types=1);

namespace Judahnator\CSV\Tests;

use Judahnator\CSV\Reader;
use PHPUnit\Framework\TestCase;
use RuntimeException;
use SplFileInfo;

/**
 * @covers \Judahnator\CSV\Reader
 */
final class ReaderTest extends TestCase
{
    private const TEST_FILE_PREFIX = 'read_test_';

    public function testConstructionException(): void
    {
        $file = new SplFileInfo(tempnam(sys_get_temp_dir(), self::TEST_FILE_PREFIX));
        chmod($file->getRealPath(), 0200); // write only

        $this->expectExceptionObject(
            new RuntimeException('The file at ' . $file->getPath() . ' is not readable!')
        );

        new Reader($file);
    }

    public function testGetIterator(): void
    {
        $file = new SplFileInfo(tempnam(sys_get_temp_dir(), self::TEST_FILE_PREFIX));
        file_put_contents(
            $file->getRealPath(),
            <<<CSV
            foo,bar
            bing,baz
            one,two
            
            CSV
        );
        $this->assertEquals(
            [
                ['foo' => 'bing', 'bar' => 'baz'],
                ['foo' => 'one', 'bar' => 'two'],
            ],
            iterator_to_array(new Reader($file))
        );
    }
}
