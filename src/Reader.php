<?php declare(strict_types=1);

namespace Judahnator\CSV;

use IteratorAggregate;
use RuntimeException;
use SplFileInfo;
use Traversable;

final class Reader implements IteratorAggregate
{
    public function __construct(private SplFileInfo $file)
    {
        if (!$this->file->isReadable()) {
            throw new RuntimeException('The file at ' . $this->file->getPath() . ' is not readable!');
        }
    }

    public function getIterator(): Traversable
    {
        $handle = fopen($this->file->getRealPath(), 'r');
        $headers = [];
        while (($data = fgetcsv($handle)) !== FALSE) {
            if (empty($headers)) {
                $headers = $data;
                continue;
            }
            yield array_combine($headers, $data);
        }
        fclose($handle);
    }
}
