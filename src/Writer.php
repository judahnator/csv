<?php declare(strict_types=1);

namespace Judahnator\CSV;

use RuntimeException;
use SplFileInfo;

final class Writer
{
    public function __construct(private SplFileInfo $file)
    {
        if (!$this->file->isWritable()) {
            throw new RuntimeException('The file at ' . $this->file->getPath() . ' is not writable!');
        }
    }

    public function write(iterable $data): void
    {
        $resource = fopen($this->file->getRealPath(), 'w');

        foreach ($data as $row) {
            fputcsv($resource, $row);
        }

        fclose($resource);
    }
}
