# Judahnator/CSV

This library is a super simple abstraction layer for working with CSV files.

## Examples

### Reading Files

```php
/*
 * path/to/file.csv -
 * foo,bar
 * bing,baz
 * one,two
 */
$reader = new \Judahnator\CSV\Reader(new SplFileInfo('path/to/file.csv'));
foreach ($reader as ['foo' => $foo, 'bar' => $bar]) {
    echo $foo, ' ', $bar, PHP_EOL;
}
/*
 * prints:
 * bing baz
 * one two
 */
```

### Writing Files

```php
$writer = new \Judahnator\CSV\Writer(new SplFileInfo('path/to/file.csv'));
$writer->write([
    ['foo', 'bar'],
    ['bing', 'baz'],
    ['one', 'two'],
]);
// The file now contains the same as the above example
```